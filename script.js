//LEITURA DOS CAMPOS
//------------------
let cadastroFuncional = document.querySelector("#cadastroFuncional");
let cadastroSenha = document.querySelector("#cadastroSenha");
let cadastroEmail = document.querySelector("#cadastroEmail");
let cadastroNome = document.querySelector("#cadastroNome");

//LEITURA DOS CAMPOS
//------------------
let loginFuncional = document.querySelector("#loginFuncional");
let loginSenha = document.querySelector("#loginSenha");

let codeAutorization;
let nomeUsuarioLogin;



//funçao que será executada quando for precionado o botão Cadastrar
let botaoNovoUsuario = document.querySelector("#btnNovoUsuario");
botaoNovoUsuario.onclick = function(){
    document.getElementById("cadastro").style.visibility = "visible";
    document.getElementById("btnNovoUsuario").style.visibility = "hidden";
    document.getElementById("btnLogar").style.visibility = "hidden";
}

//funçao que será executada quando for precionado o botão Cadastrar
let botaoCancelar = document.querySelector("#btnCancelar");
botaoCancelar.onclick = function(){
    document.getElementById("cadastro").style.visibility = "hidden";
    document.getElementById("btnNovoUsuario").style.visibility = "visible";
    document.getElementById("btnLogar").style.visibility = "visible";
}

//funçao que será executada quando for pressionado o botão Cancelar
let botaoCadastrar = document.querySelector("#btnCadastrar");
botaoCadastrar.onclick = cadastrarUsuario;

function cadastrarUsuario (){
    let dados = {
        funcional: cadastroFuncional.value,
        senha: cadastroSenha.value,
        email: cadastroEmail.value,
        nome: cadastroNome.value
    };

    fetch("http://localhost:8080/cadastrar/usuario", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json'
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
        }

        return resposta.json();
    }).then(dados => {
        console.log(dados);
    });
}

//funçao que será executada quando for pressionado o botão Cancelar
let botaoLogar = document.querySelector("#btnLogar");
botaoLogar.onclick = logarUsuario;

function logarUsuario (){
    let dados = {
        funcional: loginFuncional.value,
        senha: loginSenha.value
    };

    fetch("http://localhost:8080/login", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json'
        }
    }).then(resposta => {
        if(resposta.status !== 200){
            let resp = document.querySelector("#respostaLogar");
            let htmlErro = document.createElement("p");
            htmlErro.innerHTML = "Senha ou Usuário inválido";
            resp.appendChild(htmlErro);
            
            return;
        }
        //retorno do token
        codeAutorization = resposta.headers.get("Authorization");
        console.log ("Token " + codeAutorization);
        

        return resposta.json();
    }).then(dados => {
        console.log(dados);
        nomeUsuarioLogin =  dados.nome;
        console.log ("Nome do usuario " + nomeUsuarioLogin);
    });
}